# ics-ans-role-golang

Ansible role to install golang.

## Role Variables

```yaml
golang_version: '1.12.6'
golang_mirror: 'https://storage.googleapis.com/golang'
golang_install_dir: '/opt/go/{{ golang_version }}'
golang_download_dir: "{{ x_ansible_download_dir | default(ansible_env.HOME + '/.ansible/tmp/downloads') }}"
golang_gopath: /opt/gopath
golang_download_filename: 'go{{ golang_version }}.linux-amd64.tar.gz'
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-golang
```

## License

BSD 2-clause

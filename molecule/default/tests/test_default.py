import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_go_installed(host):
    assert host.file('/opt/go/1.12.6/bin/go').exists
    assert host.run('go')
